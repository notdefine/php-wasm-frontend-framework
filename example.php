<?php

//$stdErr = fopen('php://stderr', 'w');
//register_shutdown_function(
//    function () use ($stdErr, &$errors) {
//        fwrite($stdErr, json_encode(['session_id' => session_id()]) . "\n");
//        fwrite($stdErr, json_encode(['headers' => headers_list()]) . "\n");
//        fwrite($stdErr, json_encode(['errors' => error_get_last()]) . "\n");
//    }
//);
//
//set_error_handler(
//    function (...$args) use ($stdErr, &$errors) {
//        fwrite($stdErr, print_r($args, 1));
//    }
//);


Browser::logConsole('testmessage');

$userList = new UserList();
$userList->setUsers(['Thomas', 'Petra', 'Livia']);
$listTemplate = new UserListTemplate('userList', $userList);

$exampleEntity = new ExampleEntity();

$buttonsTemplate = new ExampleButtonRowTemplate('element1', $exampleEntity);
$formTemplate = new ExampleFormTemplate('elementIdentifier', $exampleEntity);

initApp(
    $formTemplate,
    $listTemplate,
    $buttonsTemplate
);

$userList->addUser('Karla');
$userList->publish();

$exampleEntity
    ->setValue1('Ok')
    ->setValue2('Abort!')
    ->setText('I am a tiny text');
$exampleEntity->publish();


class UserListTemplate
{
    private UserList $userList;
    private string $uuid;

    public function __construct(string $uuid, UserList $userList)
    {
        $this->userList = $userList;
        $userList->addListener($this);
        $this->uuid = $uuid;
    }

    public function update(UserList $userList): void
    {
        $this->userList = $userList;
        Browser::replaceContentOfId($this->uuid, $this->getHtml());
    }

    public function getHtml(): string
    {
        // for example rendered by twig
        $html = '<ul id="' . $this->uuid . '"  class="list-group">';

        foreach ($this->userList->getUsers() as $user) {
            $html .= '<li class="list-group-item">' . $user . '</li>';
        }

        return $html . '</ul>';
    }
}

class ExampleButtonRowTemplate
{
    private ExampleEntity $entity;
    private string $uuid;

    public function __construct(string $uuid, ExampleEntity $entity)
    {
        $this->entity = $entity;
        $entity->addListener($this);
        $this->uuid = $uuid;
    }

    public function update(ExampleEntity $value): void
    {
        $this->entity = $value;
        Browser::replaceContentOfId($this->uuid, $this->getHtml());
    }

    public function getHtml(): string
    {
        // for example rendered by twig
        return '
            <div id="' . $this->uuid . '">
                <button class="btn btn-primary">' . $this->entity->getValue1() . '</button>
                <button class="btn btn-secondary">' . $this->entity->getValue2() . '</button>
            </div>';
    }
}

class ExampleFormTemplate
{
    private ExampleEntity $entity;
    private string $uuid;

    public function __construct(string $uuid, ExampleEntity $entity)
    {
        $this->uuid = $uuid;
        $entity->addListener($this);
        $this->entity = $entity;
    }

    // for example rendered by twig
    public function update(ExampleEntity $value): void
    {
        $this->entity = $value;
        Browser::replaceContentOfId($this->uuid, $this->getHtml());
    }

    public function getHtml(): string
    {
        return '
            <div id="' . $this->uuid . '">
                <form>
                    <textarea id="textarea">' . $this->entity->getText() . '</textarea>
                </form>
            </div>';
    }
}

class UserList
{
    protected array $users;
    private array $observers = [];

    public function addListener($template): void
    {
        $this->observers[] = $template;
    }

    public function publish(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function getUsers(): array
    {
        return $this->users;
    }

    public function setUsers(array $users): UserList
    {
        $this->users = $users;
        return $this;
    }

    public function addUser(string $user): void
    {
        $this->users[] = $user;
    }
}

class ExampleEntity
{
    protected string $value1 = 'Submit';
    protected string $value2 = 'Cancel';
    protected string $text = 'example Text';

    private array $observers = [];

    public function addListener($template)
    {
        $this->observers[] = $template;
    }

    public function publish(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function getValue1(): string
    {
        return $this->value1;
    }

    public function setValue1(string $value1): ExampleEntity
    {
        $this->value1 = $value1;
        return $this;
    }

    public function getValue2(): string
    {
        return $this->value2;
    }

    public function setValue2(string $value2): ExampleEntity
    {
        $this->value2 = $value2;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): ExampleEntity
    {
        $this->text = $text;
        return $this;
    }
}
