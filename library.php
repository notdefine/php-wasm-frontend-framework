<?php

class Browser
{

    public static function logConsole($str): void
    {
        vrzno_eval('console.log("' . $str . '")');
    }


    public static function alert(string $message): void
    {
        vrzno_run('alert', [$message]);
    }


    // C&P

    public static function replaceContentOfId(string $id, string $html): void
    {
        vrzno_eval(
            "let elem = document.getElementById('" . $id . "');
        elem.innerHTML = '" . self::escapeJavaScriptText($html) . "'"
        );
        //    vrzno_eval(
//        "let elem = document.getElementById('app');
//        elem.style.background = 'red'"
//    );
    }

    private static function escapeJavaScriptText(string $string): string
    {
        return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
    }

}

function initApp(...$elemets)
{
    $html = '';
    foreach ($elemets as $element) {
        $html .= $element->getHtml();
    }
    Browser::replaceContentOfId('app', $html);
}
