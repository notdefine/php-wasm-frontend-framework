# php-wasm-frontend-framework

POC for a frontend framework in PHP. Only for experience ;)

Based on [PIB: PHP in Browser (and Node.js) aka php-wasm](https://github.com/oraoto/pib)

![Example](screens/2021-03-12%2020-22-13.png "Example")

```bash
docker run -v $PWD:/usr/share/nginx/html:ro -p 8080:80 nginx 
```

[Access via Browser](http://localhost:8080)

## Example Usage

```php
$userList = new UserList();
$userList->setUsers(['Thomas', 'Petra', 'Livia']);
$listTemplate = new UserListTemplate('userList', $userList);

$exampleEntity = new ExampleEntity();

$buttonsTemplate = new ExampleButtonRowTemplate('element1', $exampleEntity);
$formTemplate = new ExampleFormTemplate('elementIdentifier', $exampleEntity);

initApp(
    $formTemplate,
    $listTemplate,
    $buttonsTemplate
);

$userList->addUser('Karla');
$userList->publish();

$exampleEntity
    ->setValue1('Ok')
    ->setValue2('Abort!')
    ->setText('I am a tiny text');
$exampleEntity->publish();
```
