const php = new (require('./Php').Php);

php.addEventListener('output', (event) => {
  console.log(event.detail);
});

php.addEventListener('ready', () => {
  var script = document.createElement('script');
  script.type = 'text/php';
  script.src = 'library.php';
  document.getElementsByTagName('head')[0].appendChild(script);


  var script2 = document.createElement('script');
  script2.type = 'text/php';
  script2.src = 'example.php';

  document.getElementsByTagName('head')[0].appendChild(script2);

  // Call functions from php-web.data is here possible
  php.run('<?php echo "Hello, world!";').then(retVal => {
    // retVal contains the return code.
  });
});
